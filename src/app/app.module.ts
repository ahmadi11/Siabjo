import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { PresensiPage } from '../pages/presensi/presensi';
import { DispensasiPage } from '../pages/dispensasi/dispensasi';
import { GroupJadwalPage } from '../pages/group-jadwal/group-jadwal';
import { KoneksiFingerprintPage } from '../pages/koneksi-fingerprint/koneksi-fingerprint';
import { SearcPage } from '../pages/searc/searc';
import { DetailPresensiPage } from '../pages/detail-presensi/detail-presensi';
import { DetailDispensasiPage } from '../pages/detail-dispensasi/detail-dispensasi';
import { JadwalDetailPage } from '../pages/jadwal-detail/jadwal-detail';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { DatePicker } from '@ionic-native/date-picker';
import { SearcProvider } from '../providers/searc/searc';
import { GetdataProvider } from '../providers/getdata/getdata';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    DashboardPage,
    PresensiPage,
    DispensasiPage,
    KoneksiFingerprintPage,
    GroupJadwalPage,
    SearcPage,
    DetailPresensiPage,
    DetailDispensasiPage,
    JadwalDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    DashboardPage,
    PresensiPage,
    DispensasiPage,
    KoneksiFingerprintPage,
    GroupJadwalPage,
    SearcPage,
    DetailPresensiPage,
    DetailDispensasiPage,
    JadwalDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // DatePicker,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SearcProvider,
    GetdataProvider
  ]
})
export class AppModule {}
