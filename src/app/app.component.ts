import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { DashboardPage } from '../pages/dashboard/dashboard';
import { PresensiPage } from '../pages/presensi/presensi';
import { DispensasiPage } from '../pages/dispensasi/dispensasi';
// import { KoneksiFingerprintPage } from '../pages/koneksi-fingerprint/koneksi-fingerprint';
import { GroupJadwalPage }from '../pages/group-jadwal/group-jadwal';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = DashboardPage;

  pages: Array<{icon:any, title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { icon:"wifi", title: 'Dashboard' , component : DashboardPage},
      { icon:"md-clipboard", title: 'Presensi' , component: PresensiPage},
      { icon:"ios-eye-outline", title: 'Dispensasi' , component: DispensasiPage},
      { icon:"ios-eye-outline", title: 'Jadwal' , component: GroupJadwalPage},
      // { icon:"md-finger-print", title: 'Koneksi Fingerprint' , component: KoneksiFingerprintPage}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
