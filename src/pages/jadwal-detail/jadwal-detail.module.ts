import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JadwalDetailPage } from './jadwal-detail';

@NgModule({
  declarations: [
    JadwalDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(JadwalDetailPage),
  ],
})
export class JadwalDetailPageModule {}
