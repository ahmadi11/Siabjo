import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DispensasiPage } from './dispensasi';

@NgModule({
  declarations: [
    DispensasiPage,
  ],
  imports: [
    IonicPageModule.forChild(DispensasiPage),
  ],
})
export class DispensasiPageModule {}
