import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearcPage } from './searc';

@NgModule({
  declarations: [
    SearcPage,
  ],
  imports: [
    IonicPageModule.forChild(SearcPage),
  ],
})
export class SearcPageModule {}
