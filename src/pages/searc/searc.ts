import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { PresensiPage } from '../../pages/presensi/presensi';


/**
 * Generated class for the SearcPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-searc',
  templateUrl: 'searc.html',
})
export class SearcPage {
	data1:any;
	data2:any;
	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl:ViewController) {
	}
	klik(){
		console.log(this.data1+this.data2);
	}
	dismiss() {
	   let data = { 'data_a': ''+this.data1,'data_b': ''+this.data2 };
	   this.viewCtrl.dismiss(data);
	   this.navCtrl.setRoot(PresensiPage);
 	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SearcPage');
	}
	ionViewWillLeave() {
    	console.log("Looks like I'm about to leave :(");
  	}

}
