import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupJadwalPage } from './group-jadwal';

@NgModule({
  declarations: [
    GroupJadwalPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupJadwalPage),
  ],
})
export class GroupJadwalPageModule {}
