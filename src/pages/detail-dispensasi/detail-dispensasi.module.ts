import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailDispensasiPage } from './detail-dispensasi';

@NgModule({
  declarations: [
    DetailDispensasiPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailDispensasiPage),
  ],
})
export class DetailDispensasiPageModule {}
